package org.jkube.http;

public enum HttpVersion {
	HTTP1, HTTP2
}
