package org.jkube.http;

import static org.jkube.logging.Log.debug;

import java.io.IOException;
import java.io.InputStream;

import org.jkube.util.Utf8;

public class ResponseDebug implements StreamReader<String> {

	@Override
	public String read(final InputStream body) throws IOException {
		String msg = "Operation responded: "+Utf8.read(body);
		debug(msg);
		return msg;
	}
}
