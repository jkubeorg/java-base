package org.jkube.job;

public class DockerImage {

	private final String repository;
	private final String imageName;
	private final String versionTag;

	public static DockerImage of(final String imageName) {
		return new DockerImage(parseRepo(imageName), parseName(imageName), parseTag(imageName));
	}

	private static String parseRepo(final String str) {
		int pos = slashPos(str);
		return pos < 0 ? "docker.io" : str.substring(0, slashPos(str));
	}

	private static String parseName(final String str) {
		String nameTag = str.substring(slashPos(str)+1);
		int pos = nameTag.indexOf(':');
		return pos == -1 ? nameTag : nameTag.substring(0, pos);
	}

	private static String parseTag(final String str) {
		String nameTag = str.substring(slashPos(str)+1);
		int pos = nameTag.indexOf(':');
		return pos == -1 ? null : nameTag.substring(pos+1);
	}

	private static int slashPos(String str) {
		return str.lastIndexOf('/');
	}

	public DockerImage(final String repository, final String imageName, final String versionTag) {
		this.repository = repository;
		this.imageName = imageName;
		this.versionTag = versionTag;
	}

	@Override
	public String toString() {
		return repository+"/"+imageName+(versionTag == null ? "" : ":"+versionTag);
	}

	public String getRepository() {
		return repository;
	}

	public String getImageName() {
		return imageName;
	}

	public String getVersionTag() {
		return versionTag;
	}
}
