package org.jkube.job;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public interface Cluster {

	CompletableFuture<ExecutionResult> submit(JobOnCluster clusterJob);

}
