package org.jkube.entity;

import java.util.UUID;

import org.jkube.entity.collections.EntityList;
import org.jkube.entity.collections.LinkedIterable;
import org.jkube.entity.transaction.TransactionBlock;

public interface EntityEngine {

	<E extends Entity> E getByUUID(EntityType type, UUID uuid);

	<E extends Entity> LinkedIterable<E> getAll(EntityType type);

	TransactionBlock transactionBlock();

}
