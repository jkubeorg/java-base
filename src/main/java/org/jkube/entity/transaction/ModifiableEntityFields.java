package org.jkube.entity.transaction;

import org.jkube.entity.EntityField;
import org.jkube.entity.EntityFields;

public interface ModifiableEntityFields extends EntityFields {

	@Override
	<V> ModifiableEntityField<V> getField(String key, Class<V> valueClass);

	@Override
	ModifiableEntityFields getSubFields(String key);
}
