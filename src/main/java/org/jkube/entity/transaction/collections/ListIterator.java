package org.jkube.entity.transaction.collections;

import java.util.Iterator;

import org.jkube.entity.Entity;

public interface ListIterator<E extends Entity> extends Iterator<E> {

	@Override
	void remove();
	
	void insertBefore(E inserted);

	void insertAfter(E inserted);
}
