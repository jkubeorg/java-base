package org.jkube.entity.transaction;

import org.jkube.entity.EntityData;

@FunctionalInterface
public interface DataModifier<D extends ModifiableEntityData> {
	void modify(D data);
}
