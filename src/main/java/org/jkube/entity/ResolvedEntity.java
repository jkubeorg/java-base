package org.jkube.entity;

public interface ResolvedEntity extends Entity {

	@Override
	ResolvedEntityFields getFields();

}
