package org.jkube.entity.collections;

import java.util.Optional;

import org.jkube.entity.Entity;

public interface EntityList<E extends Entity> extends LinkedIterable<E> {
	
	Optional<E> getFirst();
	
	Optional<E> getLast();
}
