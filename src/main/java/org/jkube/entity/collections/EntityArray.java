package org.jkube.entity.collections;

import java.util.Optional;

import org.jkube.entity.Entity;
import org.jkube.entity.LinkedCollection;

public interface EntityArray<E extends Entity> extends LinkedIterable<E> {
	Optional<E> get(int index);
}
