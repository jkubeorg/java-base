package org.jkube.markdown.pipeline.v1;

public enum ImageColumns {
   STEP, REPO, IMAGE, TAG
}
