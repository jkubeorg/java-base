package org.jkube.markdown.pipeline.v1;

import org.jkube.codegen.Table;
import org.jkube.job.DockerImage;
import org.jkube.markdown.*;
import org.jkube.pipeline.definition.Pipeline;
import org.jkube.pipeline.definition.PipelineStep;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import static org.jkube.logging.Log.log;

public class PipelineMarkdown extends MarkdownFile {

	public static final String DOCKER_IMAGES = "Docker Images";
	public static final String PIPELINE = "Pipeline";
	public static final String CONFIGURATION = "Configuration";
	private static final String CONFIG = "config.json";
	public static final String WILDCARD = "*";

	private Table<ImageColumns> images;
	private PipelinePlantUML plantUML;

	public PipelineMarkdown(final Path path, List<String> lines) {
		super(path, lines);
		getSectionInfos();
	}

	public PipelineMarkdown(final Path path) {
		super(path);
		getSectionInfos();
	}

	public Map<String, PipelineStep> getSteps() {
		return plantUML.getSteps();
	}

	public PipelineStep getInput() {
		return plantUML.getInput();
	}

	public PipelineStep getOutput() {
		return plantUML.getOutput();
	}

	public Table<ImageColumns>  getImages() {
		return images;
	}

	public PipelinePlantUML getPlantUML() {
		return plantUML;
	}

	private void getSectionInfos() {
		parseLines();
		parseParamJsons();
		images = expectTable(ImageColumns.values(), true, DOCKER_IMAGES);
	}

	protected PipelinePumlParser createParser() {
		return new PipelinePumlParser();
	}

	protected void parseLines() {
		MarkdownElement element = expectSection(PIPELINE).expectElement(ElementType.PLANTUML);
		this.plantUML = createParser().parse(element.getLines());
	}

	protected void parseParamJsons() {
		MarkdownSection section = expectSection(CONFIGURATION);
		for (MarkdownSection subsection : section.getSubSections()) {
			// single block in section is considered config
			if (subsection.numElementsOfType(ElementType.OTHER) == 1) {
				addInput(subsection.getTitle(), CONFIG, subsection.expectElement(ElementType.OTHER).getLines());
			}
			// add single blocks in subsections
			for (MarkdownSection subsubSection : subsection.getSubSections()) {
				if (subsubSection.numElementsOfType(ElementType.OTHER) == 1) {
					addInput(subsection.getTitle(), subsubSection.getTitle(), subsubSection.expectElement(ElementType.OTHER).getLines());
				}
			}
			for (MarkdownElement element : subsection.getElements()) {
				if (element.getType().equals(ElementType.PARAGRAPH)) {
					for (MarkdownLink link : element.getLinks()) {
						addInput(subsection.getTitle(), link.getName(), loadLinkedData(link));
					}
				}
			}
		}
	}

	private void addInput(final String stepId, final String inputName, byte[] data) {
		log("Read {} bytes for input {} of step {}", data.length, inputName, stepId);
		expectStep(stepId).setConfigInput(inputName, data);
	}

	private void addInput(final String stepId, final String inputName, List<String> lines) {
		byte[] data = String.join("\n", lines).getBytes(StandardCharsets.UTF_8);
		log("Read {} lines ({} bytes) for input {} of step {}", lines.size(), data.length, inputName, stepId);
		expectStep(stepId).setConfigInput(inputName, data);
	}

	private PipelineStep expectStep(final String stepId) {
		PipelineStep res = plantUML.getSteps().get(stepId);
		if (res == null) {
			throw new MarkdownParsingException("No such step in pipeline: "+stepId);
		}
		return res;
	}

	private void show() {
		System.out.println("Parsed Markdown:");
	}

	public Pipeline createPipeline() {
		setImages();
		return new Pipeline(getMainSection().getTitle(),
				getMainSection().expectElement(ElementType.PARAGRAPH).getLines(),
				plantUML.getSteps().values(),
				plantUML.getInput(),
				plantUML.getOutput());
	}

	protected void setImages() {
		for (PipelineStep step : plantUML.getSteps().values()) {
			Map<ImageColumns, String> row = determineRow(step, images, ImageColumns.STEP);
			String repo = row.get(ImageColumns.REPO);
			String name = row.get(ImageColumns.IMAGE);
			String tag = row.get(ImageColumns.TAG);
			step.setImage(new DockerImage(repo, name, tag));
		}
	}

	protected <C> Map<C, String> determineRow(final PipelineStep step, final Table<C> table, final C stepColumn) {
		List<Map<C, String>> rows = table.findRows(stepColumn, step.getId());
		if (rows.size() > 1) {
			throw new MarkdownParsingException("Step "+step.getId()+" found twice in table");
		}
		if (rows.size() == 1) {
			return rows.get(0);
		}
		rows = table.findRows(stepColumn, WILDCARD);
		if (rows.size() > 1) {
			throw new MarkdownParsingException("Wildcard entry found twice in table");
		}
		if (rows.isEmpty()) {
			throw new MarkdownParsingException("Step "+step.getId()+" not found and no wildcard entry in table");
		}
		return rows.get(0);
	}

}
