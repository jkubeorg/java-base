package org.jkube.markdown;

public class MarkdownParsingException extends RuntimeException {
	public MarkdownParsingException(final String message) {
		super(message);
	}
}
