package org.jkube.markdown;

public enum ElementType {
	PARAGRAPH, TABLE, PLANTUML, YAML, OTHER
}
